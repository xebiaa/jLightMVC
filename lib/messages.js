var defaultLang = 'en';
var messages = {
		lookup: function(key, params) {
			var langMessages = this.messageMap[defaultLang];
			var message = langMessages[key];

			if (params)
				message = $.tmpl(message, params).text();
			return message;
		},
		messageMap:{}
};
